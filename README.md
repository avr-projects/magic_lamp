MAGIC LAMP

Projekt lampki nocnej z wykorzystaniem "Magic LEDów" - diod WS2812B. 

Lampka ma kilka trybów świecenia z możliwością ściemniania/rozjaśniania, zmiany koloru. 
Zbudowana na mikrokontrolerze Atmega8. 

W projekcie wykorzystana została biblioteka do obsługi diod WS2812B autorstwa M. Kardasia. 