/*
 * mled.h
 *
 *  Created on: 9 lip 2014
 *      Author: M
 */

#ifndef MLED_H_
#define MLED_H_

#define WSPIN1	(1<<PB0)
#define WS_PORT	PORTB
#define WS_DIR	DDRB

typedef struct {
	uint8_t r, g, b;
} LED;

typedef enum {
	false, true
} bool; // boolowski typ danych

#define NUMBER_OF_LEDS_1 30
//#define NUMBER_OF_LEDS_2 12


void strip_asm_send(void *data, uint16_t datlen, uint8_t pin);
//void clear(void * line, uint32_t color, uint32_t nr_of_leds);
//void set_pixel(void * line, int x, uint32_t color, uint32_t nr_of_leds);
//void set_line(void * line, int x, uint16_t length, uint32_t color, uint32_t nr_of_leds);
//uint32_t set_color(uint8_t r, uint8_t g, uint8_t b);
//void set_comet(void * line, int x, uint16_t length, bool rev, uint8_t r,
//		uint8_t g, uint8_t b, uint32_t nr_of_leds);
//void set_gradient(void * line, int x, uint16_t length, uint32_t color1, uint32_t color2, uint32_t nr_of_leds);

void hsv_to_rgb(LED *myrgb, uint8_t h, uint8_t s, uint8_t v);

#endif /* MLED_H_ */
