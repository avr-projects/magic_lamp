#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>

#include "mled.h"

#define LED1 (1<<PB1) // wyj�cie ta�my led
#define KEY1 (1<<PB1) // przycisk1
#define KEY2 (1<<PB2) // przycisk2

// Wykorzystanie pami�ci EEPROM aby zachowac ustawienia lampki po wy��czeniu zasilania
uint8_t EEMEM eeprom_mode;
uint8_t EEMEM eeprom_h;
uint8_t EEMEM eeprom_s;
uint8_t EEMEM eeprom_v;

typedef struct {
	volatile uint8_t *KPIN;
	uint8_t key_mask;
	uint8_t wait_time_s;
	void (*kfun1)(void);
	void (*kfun2)(void);
	uint8_t klock;
	uint8_t flag;
} TBUTTON;

volatile uint16_t Timer1;

uint8_t rotator_step = 0; 	// skok rotatora
int s = 255, v = 200;		// saturation, value
float h = 0;				// hue
uint8_t mode = 4;			// tryb �wiecenia lampki
bool dir_v = true; 			// kierunek zmiany jasno�ci

TBUTTON button1;

LED LED_strip[NUMBER_OF_LEDS_1]; // deklaracja ta�my led
uint8_t * strip = (uint8_t*) LED_strip; // wska�nik na pierwsz� diod�

void change_mode();
void my_delay(uint16_t delay);

int main(void) {

	DDRB &= ~KEY1;
	PORTB |= KEY1;

	DDRB &= ~KEY2;
	PORTB |= KEY2;

	mode = eeprom_read_byte(&eeprom_mode); // odczyt z eeprom
	if (mode < 0 || mode > 4)
		mode = 0;
	h = eeprom_read_byte(&eeprom_h); // odczyt z eeprom
	if (h < 0 || h > 255)
		h = 0;
	s = eeprom_read_byte(&eeprom_s); // odczyt z eeprom
	if (s < 0 || s > 255)
		s = 255;
	v = eeprom_read_byte(&eeprom_v); // odczyt z eeprom
	if (v < 0 || v > 255)
		v = 200;

	while (1) {
		if (!(PINB & KEY1)) {		// Obs�uga pierwszego przycisku
			_delay_ms(80);
			if (!(PINB & KEY1)) {
				mode++;
				if (mode > 4)
					mode = 0;
				_delay_ms(200);
			}
		}

		if (!(PINB & KEY2)) {		// Obs�uga drugiego przycisku
			_delay_ms(80);
			if (!(PINB & KEY2)) {
				if (mode == 0 || mode == 3) {
					h += 2;
					eeprom_write_byte(&eeprom_h, (uint8_t) h);
				} else if (mode == 1 || mode == 2 || mode == 4) {
					if (dir_v) {
						v += 5;
						if (v > 255) {
							v = 255;
							dir_v = false;
						}
					}
					if (!dir_v) {
						v -= 5;
						if (v < 30) {
							v = 30;
							dir_v = true;
						}
					}
					eeprom_write_byte(&eeprom_v, v);
				}
			}
		}

		switch (mode) {
		case 0: // reczne ustalanie koloru
			if (v != 230) {
				eeprom_write_byte(&eeprom_v, 230);
				v = 230;
			}
			if (s != 255) {
				eeprom_write_byte(&eeprom_s, 255);
				s = 255;
			}
			break;

		case 1: // bialo
			if (s != 0) {
				eeprom_write_byte(&eeprom_s, 0);
				s = 0;
			}
			break;

		case 2: // auto multikolor
			if (s != 255) {
				eeprom_write_byte(&eeprom_s, 255);
				s = 255;
			}
			h += 0.01;
			eeprom_write_byte(&eeprom_h, (uint8_t) h);
			break;

		case 3: // rotator
			if (v != 200) {
				eeprom_write_byte(&eeprom_v, 200);
				v = 200;
			}
			switch (rotator_step) {
			case 0:
				for (uint8_t j = 0; j < NUMBER_OF_LEDS_1; j++) {
					hsv_to_rgb(&LED_strip[j], (uint8_t) h, s, v);
				}
				hsv_to_rgb(&LED_strip[0], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[4], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[8], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[12], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[16], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[20], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[24], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[28], (uint8_t) h + 90, s, v);
				strip_asm_send(strip, NUMBER_OF_LEDS_1, WSPIN1);
				rotator_step++;
				break;
			case 1:
				for (uint8_t j = 0; j < NUMBER_OF_LEDS_1; j++) {
					hsv_to_rgb(&LED_strip[j], (uint8_t) h, s, v);
				}
				hsv_to_rgb(&LED_strip[1], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[5], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[9], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[13], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[17], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[21], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[25], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[29], (uint8_t) h + 90, s, v);
				strip_asm_send(strip, NUMBER_OF_LEDS_1, WSPIN1);
				rotator_step++;
				break;
			case 2:
				for (uint8_t j = 0; j < NUMBER_OF_LEDS_1; j++) {
					hsv_to_rgb(&LED_strip[j], (uint8_t) h, s, v);
				}
				hsv_to_rgb(&LED_strip[2], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[6], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[10], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[14], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[18], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[22], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[26], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[30], (uint8_t) h + 90, s, v);
				strip_asm_send(strip, NUMBER_OF_LEDS_1, WSPIN1);
				rotator_step++;
				break;
			case 3:
				for (uint8_t j = 0; j < NUMBER_OF_LEDS_1; j++) {
					hsv_to_rgb(&LED_strip[j], (uint8_t) h, s, v);
				}
				hsv_to_rgb(&LED_strip[3], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[7], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[11], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[15], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[19], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[23], (uint8_t) h + 90, s, v);
				hsv_to_rgb(&LED_strip[27], (uint8_t) h + 90, s, v);

				strip_asm_send(strip, NUMBER_OF_LEDS_1, WSPIN1);
				rotator_step = 0;
				break;
			}
			my_delay(130);
			break;

		case 4:
			for (uint8_t j = 0; j < NUMBER_OF_LEDS_1; j++) {
				hsv_to_rgb(&LED_strip[j], (uint8_t) h, s, v);
				h += 45;
			}
			strip_asm_send(strip, NUMBER_OF_LEDS_1, WSPIN1);
			my_delay(200);
			break;
		}

		if (mode != 4 && mode != 3) {
			for (int i = 0; i < NUMBER_OF_LEDS_1; i++) {
				hsv_to_rgb(&LED_strip[i], (uint8_t) h, s, v);
			}
			strip_asm_send(strip, NUMBER_OF_LEDS_1, WSPIN1);
		}
	}

}

void change_mode() {
	mode++;
	if (mode > 4) {
		mode = 0;
	}
	eeprom_write_byte(&eeprom_mode, mode); // zapis do eeprom

}

void my_delay(uint16_t delay) {

	for (int i = 0; i < delay; i++) {
		_delay_ms(1);
	}
}

