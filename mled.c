/*
 * ws281x_asm.c
 *
 *  Created on: 30-06-2014
 *      Author: Miros�aw Karda�
 *      F_CPU = 18,432 MHz
 */
#include <avr/io.h>
#include "string.h"

#include "mled.h"

//     bit = 0				    bit = 1
//  +----+        |        +--------+    |
//  |    |        |        |        |    |
//  |    |        |        |        |    |
//  |    |        |        |        |    |
//  |    |        |        |        |    |
//  |    +--------+        |        +----+
//  400ns   800ns			 800ns   400ns

void strip_asm_send(void *data, uint16_t datlen, uint8_t pin) {

	uint8_t databyte = 0, cnt, pinLO = ~pin;
	WS_DIR |= pin;
	datlen *= 3;

#if F_CPU == 12000000

	asm volatile(
			"		lds		%[cnt],%[ws_port]	\n\t"
			"		or		%[pinHI],%[cnt]		\n\t"
			"		and		%[pinLO],%[cnt]		\n\t"
			"mPTL%=:subi	%A6,1					\n\t"
			"		sbci	%B6,0				\n\t"
			"		brcs	exit%=				\n\t"
			"		ld		%[databyte],X+		\n\t"
			"		ldi		%[cnt],8		\n\t"

			"oPTL%=:sts		%[ws_port],	%[pinHI]	\n\t"
			"		nop				\n\t"

			"		lsl		%[databyte]			\n\t"
			"		brcs	.+2					\n\t"
			"		sts		%[ws_port],	%[pinLO]\n\t"
			"		nop							\n\t"

			"		dec		%[cnt]					\n\t"
			"		sts		%[ws_port],	%[pinLO]	\n\t"
			"		breq	mPTL%=					\n\t"
			"		nop					\n\t"

			"		rjmp	oPTL%=					\n\t"
			"exit%=:							\n\t"
			: [cnt]"=&d" (cnt)
			: [databyte]"r" (databyte), [ws_port]"M" (_SFR_MEM_ADDR(WS_PORT)), [pinHI]"r" (pin),
			[pinLO]"r" (pinLO), [data]"x" (data), [datlen]"r" (datlen)
	);

#endif

//#if F_CPU == 18432000
//
//	asm volatile(
//			"		lds		%[cnt],%[ws_port]	\n\t"
//			"		or		%[pinHI],%[cnt]		\n\t"
//			"		and		%[pinLO],%[cnt]		\n\t"
//			"mPTL%=:subi	%A6,1				\n\t"
//			"		sbci	%B6,0				\n\t"
//			"		brcs	exit%=				\n\t"
//			"		ld		%[databyte],X+		\n\t"
//			"		ldi		%[cnt],8			\n\t"
//
//			"oPTL%=:sts		%[ws_port],	%[pinHI]	\n\t"
//			"		rjmp .+0				\n\t"
//			"		nop						\n\t"
//
//			"		lsl		%[databyte]			\n\t"
//			"		brcs	.+2					\n\t"
//			"		sts		%[ws_port],	%[pinLO]\n\t"
//			"		rjmp .+0					\n\t"
//			"		rjmp .+0				\n\t"
//
//			"		dec		%[cnt]					\n\t"
//			"		sts		%[ws_port],	%[pinLO]\n\t"
//			"		breq	mPTL%=				\n\t"
//			"		rjmp .+0					\n\t"
//			"		rjmp .+0				\n\t"
//
//			"		rjmp	oPTL%=				\n\t"
//			"exit%=:							\n\t"
//			: [cnt]"=&d" (cnt)
//			: [databyte]"r" (databyte), [ws_port]"M" (_SFR_MEM_ADDR(WS_PORT)),
//			[pinHI]"r" (pin), [pinLO]"r" (pinLO), [data]"x" (data), [datlen]"r" (datlen)
//	);
//
//#endif
//
//#if F_CPU == 16000000
//
//	asm volatile(
//			"		lds		%[cnt],%[ws_port]	\n\t"
//			"		or		%[pinHI],%[cnt]		\n\t"
//			"		and		%[pinLO],%[cnt]		\n\t"
//			"mPTL%=:subi	%A6,1				\n\t"
//			"		sbci	%B6,0				\n\t"
//			"		brcs	exit%=				\n\t"
//			"		ld		%[databyte],X+		\n\t"
//			"		ldi		%[cnt],8			\n\t"
//
//			"oPTL%=:sts		%[ws_port],	%[pinHI]	\n\t"
//			"		rjmp .+0				\n\t"
//
//			"		lsl		%[databyte]			\n\t"
//			"		brcs	.+2					\n\t"
//			"		sts		%[ws_port],	%[pinLO]\n\t"
//			"		rjmp .+0					\n\t"
//			"		rjmp .+0				\n\t"
//
//			"		dec		%[cnt]					\n\t"
//			"		sts		%[ws_port],	%[pinLO]\n\t"
//			"		breq	mPTL%=				\n\t"
//			"		rjmp .+0					\n\t"
//
//			"		rjmp	oPTL%=				\n\t"
//			"exit%=:							\n\t"
//			: [cnt]"=&d" (cnt)
//			: [databyte]"r" (databyte), [ws_port]"M" (_SFR_MEM_ADDR(WS_PORT)),
//			[pinHI]"r" (pin), [pinLO]"r" (pinLO), [data]"x" (data), [datlen]"r" (datlen)
//	);
//
//#endif
}
//
//void clear(void * line, uint32_t color, uint32_t nr_of_leds) {
//
//	uint16_t i;
//	uint32_t NUMBER_OF_LEDS = nr_of_leds;
//
//	if (!color)
//		memset(line, 0, NUMBER_OF_LEDS * 3);
//	else {
//		for (i = 0; i < NUMBER_OF_LEDS; i++) {
//			//x=i*3;
//			*((uint8_t*) line + i * 3) = color >> 8;
//			*((uint8_t*) line + i * 3 + 1) = color >> 16;
//			*((uint8_t*) line + i * 3 + 2) = color;
//			//zamiast x by�o i*3 i w p�tli NUMBER_OF_LEDS nie by� mno�ony
//		}
//	}
//	//strip_asm_send ( (uint8_t*) line, NUMBER_OF_LEDS, WSPIN);
//}
//
//void set_pixel(void * line, int x, uint32_t color, uint32_t nr_of_leds) {
//
//	uint32_t NUMBER_OF_LEDS = nr_of_leds;
//
//	if (x < 0)
//		return;
//	if (x >= NUMBER_OF_LEDS)
//		return;
//	*((uint8_t*) line + x * 3) = color >> 8;
//	*((uint8_t*) line + x * 3 + 1) = color >> 16;
//	*((uint8_t*) line + x * 3 + 2) = color;
//}
//
//void set_line(void * line, int x, uint16_t length, uint32_t color,
//		uint32_t nr_of_leds) {
//
//	uint32_t NUMBER_OF_LEDS = nr_of_leds;
//	int i;
//	for (i = x; i < (int) (x + length); i++) {
//		set_pixel(line, i, color, NUMBER_OF_LEDS);
//	}
//}
//
//void set_comet(void * line, int x, uint16_t length, bool rev, uint8_t r,
//		uint8_t g, uint8_t b, uint32_t nr_of_leds) {
//
//	uint32_t NUMBER_OF_LEDS = nr_of_leds;
//
//	uint8_t r_tab[34];
//	uint8_t r_tab_normal[34] = { 255, 255, 250, 245, 240, 230, 220, 210, 200,
//			190, 180, 170, 160, 150, 140, 130, 120, 100, 85, 75, 60, 50, 30, 20,
//			15, 10, 5, 4, 3, 2, 1, 1, 1, 1 };
//	uint8_t r_tab_reverse[34] = { 1, 1, 1, 1, 2, 3, 4, 5, 10, 15, 20, 30, 50,
//			60, 75, 85, 100, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210,
//			220, 230, 240, 245, 250, 255, 255 };
//
//	if (rev == true)
//		memcpy(r_tab, r_tab_reverse, 34);
//	else
//		memcpy(r_tab, r_tab_normal, 34);
//	int i;
//	for (i = (int) (x + length); i > x; i--) {
//		if (r && g && b)
//			set_pixel(line, i,
//					set_color(r_tab[(int) (x + length) - i],
//							r_tab[(int) (x + length) - i],
//							r_tab[(int) (x + length) - i]), NUMBER_OF_LEDS);
//		else if (r && g)
//			set_pixel(line, i,
//					set_color(r_tab[(int) (x + length) - i],
//							r_tab[(int) (x + length) - i], 0), NUMBER_OF_LEDS);
//		else if (r && b)
//			set_pixel(line, i,
//					set_color(r_tab[(int) (x + length) - i], 0,
//							r_tab[(int) (x + length) - i]), NUMBER_OF_LEDS);
//		else if (g && b)
//			set_pixel(line, i,
//					set_color(0, r_tab[(int) (x + length) - i],
//							r_tab[(int) (x + length) - i]), NUMBER_OF_LEDS);
//		else if (r)
//			set_pixel(line, i, set_color(r_tab[(int) (x + length) - i], 0, 0),
//					NUMBER_OF_LEDS);
//		else if (g)
//			set_pixel(line, i, set_color(0, r_tab[(int) (x + length) - i], 0),
//					NUMBER_OF_LEDS);
//		else if (b)
//			set_pixel(line, i, set_color(0, 0, r_tab[(int) (x + length) - i]),
//					NUMBER_OF_LEDS);
//	}
//}
//
//void set_gradient(void * line, int x, uint16_t length, uint32_t color1,
//		uint32_t color2, uint32_t nr_of_leds) {
//	uint32_t NUMBER_OF_LEDS = nr_of_leds;
//
//	uint8_t r1 = color1 >> 16;
//	uint8_t g1 = color1 >> 8;
//	uint8_t b1 = color1;
//	uint8_t r2 = color2 >> 16;
//	uint8_t g2 = color2 >> 8;
//	uint8_t b2 = color2;
//	int i;
//	for (i = x; i < (int) (x + length); i++) {
//		if (i < 0)
//			continue;
//		if (i > NUMBER_OF_LEDS + 1)
//			continue;
//		int32_t temp_r = (r2 - r1) * (int32_t) (i - x) / (int32_t) length + r1;
//		int32_t temp_g = (g2 - g1) * (int32_t) (i - x) / (int32_t) length + g1;
//		int32_t temp_b = (b2 - b1) * (int32_t) (i - x) / (int32_t) length + b1;
//		set_pixel(line, i,
//				set_color((uint8_t) temp_r, (uint8_t) temp_g, (uint8_t) temp_b),
//				NUMBER_OF_LEDS);
//	}
//}
//
//uint32_t set_color(uint8_t r, uint8_t g, uint8_t b) {
//
//	uint32_t color = ((uint32_t) r << 16) + ((uint16_t) g << 8) + b;
//	return color;
//
//}

void hsv_to_rgb(LED *myrgb, uint8_t h, uint8_t s, uint8_t v) {
	/*
	 * H: 0=RED, 42=YELLOW, 85=GREEN, 128=AQUA, 171=BLUE, 214=MAGENTA
	 * S: 0=tylko bia�y, 255=pe�ne kolory
	 * V: 0=brak �wiat�a, 255=maksymalna jasno�c
	 */

	uint16_t region, remainder, p, q, t;

	if (s == 0)
		myrgb->r = myrgb->g = myrgb->b = v;
	else {
		region = h * 6 / 256;
		remainder = (h * 6) % 256;

		p = (v * (255 - s)) / 256;
		q = (v * (255 - (s * remainder) / 256)) / 256;
		t = (v * (255 - (s * (255 - remainder)) / 256)) / 256;

		switch (region) {
		case 0:
			myrgb->r = v;
			myrgb->g = t;
			myrgb->b = p;
			break;
		case 1:
			myrgb->r = q;
			myrgb->g = v;
			myrgb->b = p;
			break;
		case 2:
			myrgb->r = p;
			myrgb->g = v;
			myrgb->b = t;
			break;
		case 3:
			myrgb->r = p;
			myrgb->g = q;
			myrgb->b = v;
			break;
		case 4:
			myrgb->r = t;
			myrgb->g = p;
			myrgb->b = v;
			break;
		case 5:
			myrgb->r = v;
			myrgb->g = p;
			myrgb->b = q;
			break;

		}
	}
}

